import tkinter as tk
from tkinter import filedialog
from tkinter import ttk

def lire_automate(file_path):
    alphabet = []
    etats = []
    etat_initial = None
    etats_finaux = []
    transitions = []

    with open(file_path, 'r') as file:
        for i, line in enumerate(file):
            line = line.strip()
            if i == 0:
                alphabet = line[1:-1].split(',')
            elif i == 1:
                etats = list(map(int, line[1:-1].split(',')))
            elif i == 2:
                etat_initial = int(line[1:-1])
            elif i == 3:
                etats_finaux = list(map(int, line[1:-1].split(',')))
            else:
                transition = tuple(map(lambda x: x.strip(), line[1:-1].split(',')))
                transitions.append((int(transition[0]), transition[1], int(transition[2])))

    return alphabet, etats, etat_initial, etats_finaux, transitions

def afficher_tableau(transitions, etats, alphabet, frame):
    tableau = ttk.Treeview(frame, columns=['états'] + alphabet, show='headings')
    tableau.heading('états', text='États / Symboles')
    for symbole in alphabet:
        tableau.heading(symbole, text=symbole)

    transitions_etat = {etat: {sym: [] for sym in alphabet} for etat in etats}
    for transition in transitions:
        transitions_etat[transition[0]][transition[1]].append(transition[2])

    for etat, transitions_etat_etat in transitions_etat.items():
        tableau.insert('', 'end', values=[etat] + [', '.join(map(str, transitions_etat_etat[sym])) for sym in alphabet])

    tableau.pack(padx=5, pady=5)

def main():
    # Sélection automatique du fichier à lire
    file_path = filedialog.askopenfilename(filetypes=[("Fichiers texte", "*.txt")])
    if file_path:
        alphabet, etats, etat_initial, etats_finaux, transitions = lire_automate(file_path)

        # Création de la fenêtre principale
        fenetre = tk.Tk()
        fenetre.title("Automate")

        # Cadre pour le tableau
        cadre_tableau = ttk.Frame(fenetre)
        cadre_tableau.grid(row=0, column=0, padx=10, pady=10)

        # Affichage du tableau
        afficher_tableau(transitions, etats, alphabet, cadre_tableau)

        fenetre.mainloop()

if __name__ == "__main__":
    main()
