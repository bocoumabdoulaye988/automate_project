import tkinter as tk
from tkinter import filedialog, messagebox

def fichier_non_trouve(func):
    def wrapper(filename):
        try:
            return func(filename)
        except FileNotFoundError:
            messagebox.showerror("Fichier non trouvé", "Fichier spécifié introuvable.")
    return wrapper

@fichier_non_trouve
def lire_automate_depuis_fichier(filename):
    with open(filename, 'r') as file:
        lines = file.readlines()

    etats = lines[0].strip().split(', ')
    alphabet = lines[1].strip().split(', ')
    transitions = {}
    for line in lines[2:-2]:
        source, symbole, cible = line.strip().split(', ')
        transitions[(source, symbole)] = cible
    etat_initial = lines[-2].strip()
    etats_acceptants = lines[-1].strip().split(', ')

    return {'etats': etats, 'alphabet': alphabet, 'transitions': transitions, 'etat_initial': etat_initial, 'etats_acceptants': etats_acceptants}

def reconnaitre_mot(automate, mot):
    etat_courant = automate['etat_initial']
    etapes = [(automate['etat_initial'], mot)]

    for i, symbole in enumerate(mot, 1):
        if symbole not in automate['alphabet'] or (etat_courant, symbole) not in automate['transitions']:
            return False, etapes

        etat_suivant = automate['transitions'][(etat_courant, symbole)]
        etapes.append((etat_suivant, mot[i:]))

        etat_courant = etat_suivant

    return etat_courant in automate['etats_acceptants'], etapes

def charger_fichier():
    filename = filedialog.askopenfilename(filetypes=[("Fichiers texte", "*.txt")])
    if filename:
        entry_file.delete(0, tk.END)
        entry_file.insert(0, filename)
        charger_automate(filename)

def charger_automate(filename):
    global automate
    automate = lire_automate_depuis_fichier(filename)
    bouton_reconnaitre.config(state='normal')

def verifier_mot():
    global automate
    if automate is None:
        messagebox.showerror("Erreur", "Veuillez d'abord charger un automate.")
        return

    mot = entry_word.get()
    reconnu, etapes = reconnaitre_mot(automate, mot)

    if reconnu:
        label_resultat.config(text=f"Le mot '{mot}' est reconnu par l'automate.")
    else:
        label_resultat.config(text=f"Le mot '{mot}' n'est pas reconnu par l'automate.")

    afficher_etapes(etapes)

def afficher_etapes(etapes):
    canvas.delete("all")  # Clear previous drawings
    x, y = 50, 50  # Initial coordinates
    state_radius = 20  # Radius of state circles

    for i, (etat, mot_restant) in enumerate(etapes):
        # Draw state circle
        canvas.create_oval(x - state_radius, y - state_radius, x + state_radius, y + state_radius, fill="lightblue")
        canvas.create_text(x, y, text=etat)

        # Draw transition arrow
        if i < len(etapes) - 1:
            next_etat, _ = etapes[i + 1]
            canvas.create_line(x + state_radius, y, x + 100, y, arrow=tk.LAST)
            canvas.create_text(x + 50, y - 20, text=mot_restant)
            canvas.create_text(x + 100, y, text=next_etat)

        y += 100

root = tk.Tk()
root.title("Reconnaissance de mots par automate")

entry_file = tk.Entry(root, state='disabled')
entry_file.pack()

label_mot = tk.Label(root, text="Entrez un mot:")
label_mot.pack()

entry_word = tk.Entry(root)
entry_word.pack()

bouton_reconnaitre = tk.Button(root, text="Exécuter", command=verifier_mot, state='disabled')
bouton_reconnaitre.pack()

label_resultat = tk.Label(root, text="")
label_resultat.pack()

canvas = tk.Canvas(root, width=300, height=400)
canvas.pack()

automate = None

charger_fichier()

root.mainloop()
